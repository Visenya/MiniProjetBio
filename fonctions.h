#ifndef FONCTIONS
#define FONCTIONS



//----- Fonction d'Affichage -----

/*
Nom : afficheEtatGene
Principe : Afficher les états des différents gènes.
Reponse : Question 4.
*/
void afficheEtatGene(int etatIni[], char *nomGene[], int nbGene);

/*
Nom : afficherTab
Principe : Afficher les valeurs d'un tableau.
*/
void afficherTab(int tab[], int nbGene);

/*
Nom : afficheResultatEtatFuturGene
Principe : Affiche le résultat du futur état d'un gène selon son système.
Reponse : Question 6.
*/
void afficheResultatEtatFuturGene(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel);

/*
Nom : afficherMatrice
Principe : Afficher les valeurs d'une matrice.
*/
void afficherMatrice(int matrice_seuil[10][10], int nbGene);

/*
Nom : afficherNomGene
Principe : Afficher les nom des gènes présent dans un tableau.
*/
void afficherNomGene( char *nomGene[], int nbGene);








//----- Fonction de Calcul -----

/*
Nom : indiceGene
Principe : Calcul l'indice d'un gène selon son nom.
Reponse : Question 3.
*/
int indiceGene(const char gene[], char *nomGene[], int nbGene);

/*
Nom : nbreEtatSysteme
Principe : Calcul le nombre d'état possible du Système.
*/
int nbreEtatSysteme(int lvl_max[], int nbGene );

/*
Nom : lvlFutur
Principe : Permet de définir les règles régissant l'évolution du système depuis un état donné d'un gène et de l'afficher.
Reponse : Question 6.
*/
void lvlFutur(int etatIni[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel);

/*
Nom : lvlFuturTest
Principe : Permet de définir les règles régissant l'évolution du système depuis un état donné d'un gène.
Reponse : Question 6.
*/
int lvlFuturTest(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel);

/*
Nom : calculResulatEtatFuturGene
Principe : Calcul le futur état d'un gène selon son système.
Reponse : Question 7.
*/
void calculResulatEtatFuturGene(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel);

/*
Nom : nlvlFuturTest
Principe : Calcul le futur état d'un gène selon son système et selon le nombre d'itération souhaité par l'utilisateur.
Reponse : Question 7.
*/
void nlvlFuturTest(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[]);

/*
Nom : nlvlFutur
Principe : Calcul le futur état d'un gène selon son système et selon le nombre d'itération souhaité par l'utilisateur.
Reponse : Question 7.
*/
void nlvlFutur(int etatIni[], int matrice_seuil[10][10], int nbGene, int lvl_max[]);





//----- Fonction de Saisie -----

/*
Nom : saisieNbre
Principe : Permet de saisir le nombre total de gènes dans le système.
*/
void saisieNbre( int *nbGene);

/*
Nom : saisieNomGene
Principe : Permet de saisir les noms des différents gènes dans le système.
*/
void saisieNomGene( char *nomGene[], int nbGene);

/*
Nom : saisieLvlMax
Principe : Permet de saisir le niveau maximun de chaque gènes dans le système.
*/
void saisieLvlMax(int lvl_max[], int nbGene);

/*
Nom : saisieEtat
Principe : Permet de saisir l'état initial de chaque gènes dans le système.
*/
void saisieEtat( int etatIni[], int nbGene, int lvl_max[]);

/*
Nom : saisieMatriceArc
Principe : Permet de saisir le seuil et le signe des interactions de chaque gènes dans le système.
*/
void saisieMatriceArc( int matrice_seuil[10][10], char *nomGene[], int nbGene);



//----- Fonction de Utilitaire -----

/*
Nom : copieTableau
Principe : Permet de copier les valeurs d'un tableau dans un autre tableau de même dimension.
*/
void copieTableau(int tabIni[], int tabFinal[], int nbGene);

/*
Nom : menu
Principe : Création d'un menu.
*/
void menu(int *nbGene, char *nomGene[], int lvl_max[], int etatIni[], int matrice_seuil[10][10]);

/*
Nom : menu
Principe : Création d'un sous-menu.
*/
void sousMenu2(int *nbGene, char *nomGene[], int lvl_max[], int etatIni[], int matrice_seuil[10][10]);

/*
Nom : menu
Principe : Création d'un sous-menu.
*/
void sousMenu3(int *nbGene, char *nomGene[], int lvl_max[], int etatIni[], int matrice_seuil[10][10]);


#endif
