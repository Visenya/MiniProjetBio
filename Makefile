all: Projet

Projet: fonctions.o main.o
	gcc -Wall -o Projet fonctions.o main.o

fonctions.o: fonctions.c
	gcc -Wall -o fonctions.o -c fonctions.c

main.o: main.c fonctions.h
	gcc -Wall -o main.o -c main.c

clean:
	rm *~ *.o Projet
