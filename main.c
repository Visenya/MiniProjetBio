#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "fonctions.h"


int main(int argc, char const *argv[]) {

	// Variable

	int nbGene = 10;
	char *nomGene[] = {"Chell", "GLaDOS", "Wheatley", "RatMan", "a","b","c","d","e","f"};
	int lvl_max[10] = {0};
	int etatIni[10] = {0};
	int matrice_seuil[10][10] = { {0,0,0,0,0,0,0,0,0}} ;

	//char gene[] = "Wheatley";
	//hodor = indiceGene(gene, nomGene, nbGene);
	//afficheEtatGene(etatIni, nomGene, nbGene);
	//hodor = nbreEtatSysteme(lvl_max, nbGene);
	//afficherMatrice(matrice_seuil, nbGene );
	//printf("-------1-------------\n");
	//afficherTab(etatIni, nbGene);
	// printf("----------1bis----------\n");
	// afficheResultatEtatFuturGene(etatIni, matrice_seuil, nbGene, lvl_max, 2);
	// printf("------------aff--------\n");
	// calculResulatEtatFuturGene(etatIni, matrice_seuil, nbGene, lvl_max, 2);
	//nlvlFuturTest(etatIni, matrice_seuil, nbGene, lvl_max);
	//printf("%s\n", nomGene[2] );
	//int truc[] = {0,0,0,0};
	//copieTableau(etatIni, truc, nbGene);
	//printf("--------cal------------\n");
	//afficherTab(etatIni, nbGene);
	//afficherTab(truc, nbGene);
	//printf("%d\n", hodor);

	//Question Pourquoi sans pointeur la valeur de mon tableau change.
// si absence de copie, le tabini est modifié par conseqguent impossible de relancer une operation

	// Menu
	printf("------------------------------------------------------\n" );
	printf("Bienvenue dans l'application Genetics Network Simulator.\n");
	printf("------------------------------------------------------\n" );
	printf("\n");
	printf("Afin de veiller au bon fonctionnement de l'application, chaque donnée doit être inscrite consciencement et minutieusement. De plus, veiller respecter l'ordre des données, c'est à dire que les données devront être saisi dans le même ordre. Merci de votre compréhension.\n");
	printf("\n");
	printf("Il est conseiller de commencer la saisi des données, puis de les vérifier et enfin de procéder aux calculs souhaités.\n");
	printf("\n \n");
	printf("=========================\n");
	printf("1. Saisie \n");
	printf("2. Vérification \n");
	printf("  1) Afficher le nombre de gènes.\n");
	printf("  2) Afficher les noms des gènes.\n");
	printf("  3) Afficher les états initiaux des gènes.\n");
	printf("  4) Afficher les niveaux maximums des gènes.\n");
	printf("  5) Afficher la matrice des arcs.\n");
	printf("3. Calcul \n");
	printf("  1) Nombre total d'états possibles du système.\n");
	printf("  2) Afficher le niveau futur d'un gène selon l'interaction du système.\n");
	printf("  3) Afficher le niveau futur du système pour n fois .\n");
	printf("0. Quitter \n");
	printf("=========================\n\n");

	printf("Quel est le nombre de gènes de votre système ?\n" );
	scanf("%i",&nbGene);

	menu(&nbGene, nomGene, lvl_max, etatIni, matrice_seuil);

	return 0;
}
