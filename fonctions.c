#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "fonctions.h"




//----- Fonction d'Affichage -----

void afficheEtatGene(int etatIni[], char *nomGene[], int nbGene){
	printf("< ");
	for (int i = 0; i < 4; i++) {
		if (i == nbGene -1) {
			printf("%s = %i ", nomGene[i], etatIni[i]);
		}
		else{
			printf("%s = %i, ", nomGene[i], etatIni[i]);
		}
	}
	printf("> \n");
}

void afficherTab(int tab[], int nbGene){
	for (int i = 0; i < nbGene; ++i){
		printf("%i\t", tab[i] );
	}
	putchar('\n');
}

void afficheResultatEtatFuturGene(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel){
	int cpt = 0;
	cpt = lvlFuturTest(etat, matrice_seuil, nbGene, lvl_max, geneActuel);
	if (cpt > 0) {
		if (etat[geneActuel] + 1 >= lvl_max[geneActuel]){
			printf("L'état final est %i. \n", lvl_max[geneActuel]);
		} else{
			printf("L'état final est %i. \n", etat[geneActuel] + 1);
		}
	}
	else{
		if (cpt < 0) {
			if (etat[geneActuel] - 1 <= 0) {
				printf("L'état final est 0. \n");
			} else {
				printf("L'état final est %i. \n", etat[geneActuel] - 1);
			}
		} else{
			printf("L'état final est %i. \n", etat[geneActuel]);
		}
	}
	//afficherTab(etat,nbGene);
}

void afficherMatrice(int matrice_seuil[10][10], int nbGene){
	for (int i = 0; i < nbGene; ++i){
		for (int j = 0; j < nbGene; ++j){
			printf("%d\t", matrice_seuil[i][j]);
		}
		putchar('\n');
	}
}

void afficherNomGene( char *nomGene[], int nbGene){
	for (int i = 0; i < nbGene; ++i){
		printf("%s\n", nomGene[i] );
	}
}





//----- Fonction de Calcul -----

int indiceGene(const char gene[], char *nomGene[], int nbGene){
	int i = -1;
	while(strcmp(gene, nomGene[i]) != 0 && i < nbGene){
		i++;
	}
	return i;
}

int nbreEtatSysteme(int lvl_max[], int nbGene ){
	int cpt = 1;
	for (int i = 0; i < nbGene; i++) {
		cpt = cpt * (lvl_max[i] + 1);
	}
	return cpt;
}

void lvlFutur(int etatIni[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel){
	int cpt = 0;
	bool augmente = false;
	bool diminue = false;
	for (int i = 0; i < nbGene; i++) {
		if (matrice_seuil[i][geneActuel] != 0 ) {
			if (matrice_seuil[i][geneActuel] > 0 ) {
				if (etatIni[i] >= abs(matrice_seuil[i][geneActuel])) {
					cpt += 1;
					augmente = true;
				}	else {
					cpt -= 1;
					diminue = true;
				}
			} else{
				if (etatIni[i] >= abs(matrice_seuil[i][geneActuel])) {
					cpt -= 1;
					diminue = true;
				} else{
					cpt += 1;
					augmente = true;
				}
			}
		}
	}
	if (augmente && diminue) {
		printf("Aucun changement possible d'état, une augmentation et une diminution ont lieu, par conséquent son état est de %i .\n", etatIni[geneActuel]);
	} else{
		if (cpt > 0) {
			if (etatIni[geneActuel] + 1 >= lvl_max[geneActuel]){
				printf("L'état final est %i. \n", lvl_max[geneActuel]);
			} else{
				printf("L'état final est %i. \n", etatIni[geneActuel] + 1);
			}
		} else{
			if (cpt < 0){
	 			if (etatIni[geneActuel] - 1 <= 0) {
					printf("L'état final est 0. \n");
				} else {
					printf("L'état final est %i. \n", etatIni[geneActuel] - 1);
				}
			} else{
				printf("L'état final est %i.\n", etatIni[geneActuel]);
			}
		}
	}
}

int lvlFuturTest(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel){
	int cpt = 0;

	// printf("-------\n");
	// afficherTab(etat,nbGene);
	// printf("-------\n");

	for (int i = 0; i < nbGene; i++) {
		if (matrice_seuil[i][geneActuel] > 0 ) {
			if (etat[i] >= abs(matrice_seuil[i][geneActuel])) {
					cpt += 1;
			}	else {
					cpt -= 1;
			}
		} else {
				if (matrice_seuil[i][geneActuel] < 0) {
					if (etat[i] >= abs(matrice_seuil[i][geneActuel])) {
						cpt -= 1;
					} else {
						cpt += 1;
					}
				}
		}
	}
	return cpt;
}

void calculResulatEtatFuturGene(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[], int geneActuel){
	int cpt = 0;
	cpt = lvlFuturTest(etat, matrice_seuil, nbGene, lvl_max, geneActuel);

	// printf("--------tabtempavants--------------\n");
	// afficherTab(etatTemporaire, nbGene);

	if (cpt > 0) {
		if (etat[geneActuel] + 1 >= lvl_max[geneActuel]){
			etat[geneActuel] = lvl_max[geneActuel];
		} else{
			etat[geneActuel] += 1;
		}
	}
	else{
		if (cpt < 0) {
			if (etat[geneActuel] - 1 <= 0) {
				etat[geneActuel] = 0;
			} else {
				etat[geneActuel] -= 1;
			}
		}
	}
	// printf("--------tabtiniapres--------------\n");
	// afficherTab(etat,nbGene);
}



void nlvlFutur(int etatIni[], int matrice_seuil[10][10], int nbGene, int lvl_max[]) {
	int nbre = 0;
	int cpt;
	bool augmente = false;
	bool diminue = false;
	int etatTemporaire[nbGene];

	printf("Nombre d'état successeur souhaité ? \n" );

	scanf("%d", &nbre);

	for (int i = 0; i < nbre; i++) {
		copieTableau(etatIni, etatTemporaire, nbGene);
		afficherTab(etatTemporaire, nbGene);
		for (int ligne = 0; ligne < nbGene; ligne++) {
			cpt = 0;
			for (int colonne = 0; colonne < nbGene; colonne++){
				if (matrice_seuil[ligne][colonne] > 0 ) {
					if (etatIni[ligne] >= abs(matrice_seuil[ligne][colonne])) {
							cpt += 1;
							augmente = true;
					}	else {
							cpt -= 1;
							diminue = true;
					}
				} else {
						if (matrice_seuil[ligne][colonne] < 0) {
							if (etatIni[ligne] >= abs(matrice_seuil[ligne][colonne])) {
								cpt -= 1;
								diminue = true;
							} else {
								cpt += 1;
								augmente = true;
							}
						}
				}
			}
			if (!augmente && !diminue){
				if (cpt > 0) {
					if (etatIni[ligne] + 1 >= lvl_max[ligne]){
						etatIni[ligne] = lvl_max[ligne];
					} else{
						etatIni[ligne] = etatIni[ligne] + 1;
					}
				}
				else{
					if (cpt < 0)
					{
						if (etatIni[ligne] - 1 <= 0) {
							etatIni[ligne] = 0;
						}
						else {
							etatIni[ligne] = etatIni[ligne] - 1;
						}
					}
				}
			}
		}
	}
}


void nlvlFuturTest(int etat[], int matrice_seuil[10][10], int nbGene, int lvl_max[]) {
	int nbre = 0;
	int etatTemporaire[4]={0};

	printf("Nombre d'état successeur souhaité ? \n" );

	scanf("%d", &nbre);
	copieTableau(etat, etatTemporaire, nbGene);
	//printf("------Etatini------\n");

	for (int i = 0; i < nbre; i++) {
		// afficherTab(etat, nbGene);
		// printf("------EtatTemp------\n");
		// afficherTab(etatTemporaire, nbGene);
		// printf("------Hodor------\n");
		for (int ligne = 0; ligne < nbGene; ligne++) {
			calculResulatEtatFuturGene(etatTemporaire, matrice_seuil, nbGene, lvl_max, ligne);
			// printf("------EtatTempCours------\n");
			// afficherTab(etatTemporaire, nbGene);
		}
	}
	printf("------EtatTempFinal------\n");
	afficherTab(etatTemporaire, nbGene);
}

//----- Fonction de Saisie -----

void saisieNbre(int *nbGene){
  char *p;
  char s[100];
  int n = 0;

  printf("Veuillez saisir un nombre représentant le nombre totale de gènes dans le système.\n");
  while (fgets(s, sizeof(s), stdin)) {
      n = strtol(s, &p, 10);
      if (p == s || *p != '\n') {
          printf("Veuillez saisir un entier.\n");
      } else if (n <= 0 || n > 10) {
          printf("Veuillez saisir un entier entre 1 et 10 compris.\n");
      } else break;
  }
  nbGene = n;
  printf("Vous avez saisie : %d\n", nbGene);
}

//IMPORTATNT nomGene DOIT ÊTRE AU MAXIMUN DE SA TAILLE PRÉVU.
void saisieNomGene( char *nomGene[], int nbGene){
	printf("Veuillez saisir le nom de chaque gène du système.\n");
	for ( int i = 0; i < nbGene; i++) {
    printf("Le gène d'indice %d est : ", i );
    nomGene[i] = calloc(100, sizeof(char));
    scanf("%s",nomGene[i]);
	}
}

void saisieLvlMax(int lvl_max[], int nbGene){
	char *p;
  char s[100];

	printf("Veuillez saisir le niveau maximun de chaque gène du système. \n");
	for (int i = 0; i < nbGene; i++) {
		printf("Valeur : " );
		while (fgets(s, sizeof(s), stdin)) {
	      lvl_max[i] = strtol(s, &p, 10);
	      if (p == s || *p != '\n') {
	          printf("Veuillez saisir un entier.\n");
	      } else if (lvl_max[i] <= 0 ) {
	        printf("Veuillez saisir un entier supérieur à 0.\n");
	      } else break;
	  }
	  printf("Le gène d'indice %d est : %d\n", i, lvl_max[i]);
	}
}

void saisieEtat(int tabIni[], int nbGene, int lvl_max[]){
	char *p;
  char s[100];

	printf("Veuillez saisir l'état initial de chaque gène du système \n");
	for (int i = 0; i < nbGene; i++) {
		printf("Valeur : " );
		while (fgets(s, sizeof(s), stdin)) {
	      tabIni[i] = strtol(s, &p, 10);
	      if (p == s || *p != '\n') {
	          printf("Veuillez saisir un entier.\n");
	      } else if (tabIni[i] < 0 || tabIni[i] > lvl_max[i]) {
	        printf("Veuillez saisir un entier entre 0 et son niveau max.\n");
	      } else break;
	  }
	  printf("Le gène d'indice %d est : %d\n", i, tabIni[i]);
	}
}

void saisieMatriceArc( int matrice_seuil[10][10], char *nomGene[], int nbGene){
	char *p;
	char s[100];
	//memset(matrice_seuil, 0 , sizeof(nbGene));
	for (int i = 0; i < nbGene; i++) {
		for (int j = 0; j < nbGene; j++) {
			if (i == j) {
				matrice_seuil[i][j] = 0;
			}
			else{
				printf("Veuillez saisir l'action du %s sur %s.\n", nomGene[i], nomGene[j]);
				printf("Valeur : " );
				while (fgets(s, sizeof(s), stdin)) {
						matrice_seuil[i][j] = strtol(s, &p, 10);
						if (p == s || *p != '\n') {
								printf("Veuillez saisir un entier.\n");
						} else break;
				}
				//printf("Le gène d'indice [%d][%d] est : %d\n", i, j, matrice_seuil[i][j]);
			}
		}
	}
}





//----- Fonction de Utilitaire -----

void copieTableau(int tabIni[], int tabFinal[], int nbGene){
	for (int i = 0; i < nbGene; i++) {
		tabFinal[i] = tabIni[i];
	}
}

void menu(int *nbGene, char *nomGene[], int lvl_max[], int etatIni[], int matrice_seuil[10][10]){
	int selection = 1;
	int choix = 0;
	while(selection){
		printf("Selectionnez l'action désirée.\n");
		scanf("%i",&choix);

		switch(choix){
			case 0 :
				printf("Hodor\n");
				selection = 0;
				break;
			case 1 :
				saisieNbre( *nbGene);
				saisieNomGene( nomGene, *nbGene);
				saisieLvlMax( lvl_max, nbGene);
				saisieEtat( etatIni, nbGene, lvl_max);
				saisieMatriceArc( matrice_seuil, nomGene, nbGene);
				break;
			case 2 :
				sousMenu2(&nbGene, nomGene, lvl_max, etatIni, matrice_seuil);
				break;
			case 3 :
				sousMenu3(&nbGene, nomGene, lvl_max, etatIni, matrice_seuil);
				break;
			default :
				printf("Erreur de saisie.\n");
				break;
		}
	}
}

// Rajouter prametre
void sousMenu2(int *nbGene, char *nomGene[], int lvl_max[], int etatIni[], int matrice_seuil[10][10]){
	int selection = 1;
	int choix = 0;

	while(selection){
		printf("Selectionnez l'action désirée, faites 0 pour quitter le sous-menu.\n");
		scanf("%i",&choix);

		switch(choix){
			case 0 :
				printf("Retour au menu principal\n");
				selection = 0;
				break;
			case 1 :
				printf("Le nombre de gène est de %i.\n", nbGene);
				break;
			case 2 :
				afficherNomGene(nomGene, nbGene);
				break;
			case 3 :
				afficheEtatGene(etatIni, nomGene, nbGene);
				break;
			case 4 :
				afficherTab(lvl_max, nbGene);
				break;
			case 5 :
				afficherMatrice(matrice_seuil, nbGene);
				break;
			default :
				printf("Erreur de saisie.\n");
				printf("  0) Quitter.\n");
				printf("  1) Afficher le nombre de gènes.\n");
				printf("  2) Afficher les noms des gènes.\n");
				printf("  3) Afficher les états initiaux des gènes.\n");
				printf("  4) Afficher les niveaux maximums des gènes.\n");
				printf("  5) Afficher la matrice des arcs.\n");
				break;
		}
	}
}

void sousMenu3(int *nbGene, char *nomGene[], int lvl_max[], int etatIni[], int matrice_seuil[10][10]){
	int selection = 1;
	int choix = 0;
 	int geneActuel = -1;
	char gene[] = "";

	while(selection){
		printf("Selectionnez l'action désirée, faites 0 pour quitter le sous-menu.\n");
		scanf("%i",&choix);

		switch(choix){
			case 0 :
				printf("Retour au menu principal\n");
				selection = 0;
				break;
			case 1 :
				nbreEtatSysteme(lvl_max, nbGene);
				break;
			case 2 :
				printf("Le nom du gène étudié ?\n");
				scanf("%s\n", &gene);
				geneActuel = indiceGene(gene, nomGene, nbGene);
				lvlFutur(etatIni, matrice_seuil, nbGene, lvl_max, geneActuel);
				break;
			case 3 :
				nlvlFutur(etatIni, matrice_seuil, nbGene, lvl_max);
				break;
			default :
				printf("Erreur de saisie.\n");
				printf("  0) Quitter.\n");
				printf("  1) Nombre total d'états possibles du système.\n");
				printf("  2) Afficher le niveau futur d'un gène selon l'interaction du système.\n");
				printf("  3) Afficher le niveau futur du système pour n fois .\n");
				break;
		}
	}
}
